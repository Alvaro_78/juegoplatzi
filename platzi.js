let teclas = {
  UP: 38,
  DOWN: 40,
  LEFT:37,
  RIGHT:39
}

document.addEventListener("keydown",dibujarTeclado)
let vp = document.getElementById("villaplatzi")
let papel = vp.getContext("2d") 
let x = 420
let y = 420


let fondo = {
  url:"tile.png",
  cargarOK: false
}

let vaca = {
  url:"vaca.png",
  cargarOK: false
}

// let cerdo = {
//   url:"cerdo.png",
//   cargarOK: false
// }

let pollo = {
  url:"pollo.png",
  cargarOK: false
}

let haski = {
  url:"haski.png",
  cargarOK: false
}

fondo.imagen = new Image()
fondo.imagen.src = fondo.url
fondo.imagen.addEventListener("load",cargarFondo)

vaca.imagen = new Image()
vaca.imagen.src = vaca.url
vaca.imagen.addEventListener("load",cargarVacas)

haski.imagen = new Image()
haski.imagen.src = haski.url
haski.imagen.addEventListener("load",cargarHaskis)

// cerdo.imagen = new Image()
// cerdo.imagen.src = cerdo.url
// cerdo.imagen.addEventListener("load",cargarCerdos)

pollo.imagen = new Image()
pollo.imagen.src = pollo.url
pollo.imagen.addEventListener("load",cargarPollos)


function cargarFondo(){
          
  fondo.cargarOK = true
  dibujar()
}

function cargarHaskis(){
    haski.cargarOK = true
    dibujar()
  }
          
function cargarVacas(){
  
  vaca.cargarOK = true
  dibujar()
}


// function cargarCerdos(){
//   cerdo.cargarOK = true
//   dibujar()
// }

function cargarPollos(){
  pollo.cargarOK = true
  dibujar()
}

function dibujarTeclado(evento){

  let huski
  let movimiento = 10
  
  switch(event.keyCode){
          
    case teclas.UP:
      cargarHaskis(huski,x,y,x,y - movimiento,papel)
      y = y - movimiento
    break;
          
    case teclas.DOWN:
      cargarHaskis(huski,x,y,x,y - movimiento,papel)
      y = y + movimiento
    break;      
          
    case teclas.LEFT:
      cargarHaskis(huski,x,y,x,y - movimiento,y,papel)
      x = x - movimiento
    break;      
          
    case teclas.RIGHT:
      cargarHaskis(huski,x,y,x,y - movimiento,y,papel)
      x = x + movimiento
    break;      
                          
  }
          
}

function dibujar(){
  
  if(fondo.cargarOK){
    
    papel.drawImage(fondo.imagen, 0, 0)
  }

  if(haski.cargarOK){
        
    papel.drawImage(haski.imagen,x,y)
    
  }
  
  // if(cerdo.cargarOK){
    
  //   papel.drawImage(cerdo.imagen,x ,y)
  // }
  
  if(pollo.cargarOK){
    
    papel.drawImage(pollo.imagen,300 ,250)
    papel.drawImage(pollo.imagen, 40 ,150)

  }

  if(vaca.cargarOK){
    
    papel.drawImage(vaca.imagen, 400 , 50)
    papel.drawImage(vaca.imagen, 200 , 300)
    papel.drawImage(vaca.imagen, 100 , 90)
    papel.drawImage(vaca.imagen, 35 , 220)
    papel.drawImage(vaca.imagen, 100 , 300)
    papel.drawImage(vaca.imagen, 70 , 410)
    papel.drawImage(vaca.imagen, 100 , 300)
    papel.drawImage(vaca.imagen, 100 , 200)      
    
  }
  
  
}




